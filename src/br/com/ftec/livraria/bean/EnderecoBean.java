package br.com.ftec.livraria.bean;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import br.com.ftec.livraria.dao.DAO;
import br.com.ftec.livraria.modelo.Endereco;

@ManagedBean
@ViewScoped
public class EnderecoBean {

	private Endereco endereco = new Endereco();	
	private Integer enderecoId;

	public Endereco getEndereco() {
		return endereco;
	}

	public List<Endereco> getEnderecos() {
		
		System.out.println(">>>>>>>>>>>>>>>>>>ENDERECOS::::" + new DAO<Endereco>(Endereco.class).listaTodos().get(0).getBairro_end());
		return new DAO<Endereco>(Endereco.class).listaTodos();
	}

	public String gravar() {
		
		System.out.println("Gravando endereco " + this.endereco.getLogradouro());

		if (this.endereco.getId_endereco() == null) {
			
			new DAO<Endereco>(Endereco.class).adiciona(this.endereco);
			
		} else {
			
			new DAO<Endereco>(Endereco.class).atualiza(this.endereco);
		}

		this.endereco = new Endereco();

		return "livro?faces-redirect=true";
	}

	public void carregar(Endereco endereco) {
		System.out.println("Carregando endereco");
		this.endereco = endereco;
	}

	public void remover(Endereco endereco) {
		
		System.out.println("Removendo endereco");
		new DAO<Endereco>(Endereco.class).remove(endereco);
		
	}


	
	public void carregarEnderecoPelaId() {
		this.endereco = new DAO<Endereco>(Endereco.class).buscaPorId(enderecoId);
	}
}
