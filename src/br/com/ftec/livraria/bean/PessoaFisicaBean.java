package br.com.ftec.livraria.bean;

import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import br.com.ftec.livraria.dao.DAO;
import br.com.ftec.livraria.modelo.Endereco;
import br.com.ftec.livraria.modelo.PessoaFisica;

@ManagedBean
@ViewScoped
public class PessoaFisicaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private PessoaFisica pessoaFisica = new PessoaFisica();
	private Integer pessoaFisicaId;
	private Integer enderecoId;

	/**
	 * @return the pessoaFisicaId
	 */
	public Integer getPessoaFisicaId() {

		return pessoaFisicaId;
	}

	/**
	 * @param pessoaFisicaId the pessoaFisicaId to set
	 */
	public void setPessoaFisicaId(Integer pessoaFisicaId) {
		this.pessoaFisicaId = pessoaFisicaId;
	}

	/**
	 * @return the enderecoId
	 */
	public Integer getEnderecoId() {
		return enderecoId;
	}

	/**
	 * @param enderecoId the enderecoId to set
	 */
	public void setEnderecoId(Integer enderecoId) {
		this.enderecoId = enderecoId;
	}

	public List<PessoaFisica> getPessoasFisicas() {

		return new DAO<PessoaFisica>(PessoaFisica.class).listaTodos();
	}

	public List<Endereco> getEnderecos() {

		return new DAO<Endereco>(Endereco.class).listaTodos();

	}

	public List<Endereco> getEnderecosPessoa() {

		return this.getPessoaFisica().getEnderecos();
	}

	public void gravarEndereco() {

		Endereco endereco = new DAO<Endereco>(Endereco.class).buscaPorId(this.enderecoId);
		this.getPessoaFisica().adicionaEndereco(endereco);
		System.out.println("Endereco adicionada: " + endereco.getLogradouro());

	}

	public void gravar() {

		System.out.println("Gravando pessoa fisica " + this.getPessoaFisica().getNome_pessoa());

		if (this.getPessoaFisica().getId_pessoa_fisica() == null) {

			new DAO<PessoaFisica>(PessoaFisica.class).adiciona(this.getPessoaFisica());

		} else {

			new DAO<PessoaFisica>(PessoaFisica.class).atualiza(this.getPessoaFisica());
		}

		this.pessoaFisica = new PessoaFisica();
	}

	public void carregar(PessoaFisica pessoaFisica) {

		System.out.println("Carregando pessoaFisica " + pessoaFisica.getNome_pessoa());
		this.pessoaFisica = pessoaFisica;
	}

	public void remover(PessoaFisica pessoaFisica) {
		System.out.println("Removendo pessoaFisica " + pessoaFisica.getNome_pessoa());
		new DAO<PessoaFisica>(PessoaFisica.class).remove(pessoaFisica);
	}

	public void removeEnderecoPessoa(Endereco endereco) {
		this.getPessoaFisica().removeEndereco(endereco);
	}

	public String formAutor() {
		System.out.println("Chamanda do formulário do Autor.");
		return "autor?faces-redirect=true";
	}

	public void validaCPF(FacesContext fc, UIComponent component, Object value) throws ValidatorException {

		String valor = value.toString();
		if (!valor.startsWith("1")) {
			throw new ValidatorException(new FacesMessage("ISBN deveria começar com 1"));
		}

	}

	public void carregarPessoaPelaId() {

		this.pessoaFisica = new DAO<PessoaFisica>(PessoaFisica.class).buscaPorId(pessoaFisicaId);
	}

	/**
	 * @return the pessoaFisica
	 */
	public PessoaFisica getPessoaFisica() {
		return pessoaFisica;
	}


}
