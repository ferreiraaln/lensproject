package br.com.ftec.livraria.modelo;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Endereco implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Integer id_endereco;
	private String tipo_end;
	private String logradouro;
	private String endereco;
	private Integer num_end;
	private String cep_end;
	private String bairro_end;
	private String cidade;
	private String uf;
	private String std_end;
	/**
	 * @return the id_endereco
	 */
	public Integer getId_endereco() {
		return id_endereco;
	}
	/**
	 * @param id_endereco the id_endereco to set
	 */
	public void setId_endereco(Integer id_endereco) {
		this.id_endereco = id_endereco;
	}
	/**
	 * @return the tipo_end
	 */
	public String getTipo_end() {
		return tipo_end;
	}
	/**
	 * @param tipo_end the tipo_end to set
	 */
	public void setTipo_end(String tipo_end) {
		this.tipo_end = tipo_end;
	}
	/**
	 * @return the logradouro
	 */
	public String getLogradouro() {
		return logradouro;
	}
	/**
	 * @param logradouro the logradouro to set
	 */
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	/**
	 * @return the endereco
	 */
	public String getEndereco() {
		return endereco;
	}
	/**
	 * @param endereco the endereco to set
	 */
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	/**
	 * @return the num_end
	 */
	public Integer getNum_end() {
		return num_end;
	}
	/**
	 * @param num_end the num_end to set
	 */
	public void setNum_end(Integer num_end) {
		this.num_end = num_end;
	}
	/**
	 * @return the cep_end
	 */
	public String getCep_end() {
		return cep_end;
	}
	/**
	 * @param cep_end the cep_end to set
	 */
	public void setCep_end(String cep_end) {
		this.cep_end = cep_end;
	}
	/**
	 * @return the bairro_end
	 */
	public String getBairro_end() {
		return bairro_end;
	}
	/**
	 * @param bairro_end the bairro_end to set
	 */
	public void setBairro_end(String bairro_end) {
		this.bairro_end = bairro_end;
	}
	/**
	 * @return the cidade
	 */
	public String getCidade() {
		return cidade;
	}
	/**
	 * @param cidade the cidade to set
	 */
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	/**
	 * @return the uf
	 */
	public String getUf() {
		return uf;
	}
	/**
	 * @param uf the uf to set
	 */
	public void setUf(String uf) {
		this.uf = uf;
	}
	/**
	 * @return the std_end
	 */
	public String getStd_end() {
		return std_end;
	}
	/**
	 * @param std_end the std_end to set
	 */
	public void setStd_end(String std_end) {
		this.std_end = std_end;
	}
		
	
}
