package br.com.ftec.livraria.modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class PessoaFisica implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Integer id_pessoa_fisica;
	private String nome_pessoa;
	private String cpf_pessoa;
	private String reg_pessoa;
	private String nat_pessoa;
	private String std_civil_pessoa;

	@Temporal(TemporalType.DATE)
	private Calendar dta_nsc_pessoa = Calendar.getInstance();

	@ManyToMany(fetch = FetchType.EAGER)
	private List<Endereco> enderecos = new ArrayList<Endereco>();

	public List<Endereco> getEnderecos() {
		return enderecos;
	}

	public void adicionaEndereco(Endereco endereco) {
		this.enderecos.add(endereco);
	}

	public PessoaFisica() {
		// TODO Auto-generated constructor stub
	}

	public void removeEndereco(Endereco endereco) {
		this.enderecos.remove(endereco);
	}

	/**
	 * @return the id_pessoa_fisica
	 */
	public Integer getId_pessoa_fisica() {
		return id_pessoa_fisica;
	}

	/**
	 * @param id_pessoa_fisica the id_pessoa_fisica to set
	 */
	public void setId_pessoa_fisica(Integer id_pessoa_fisica) {
		this.id_pessoa_fisica = id_pessoa_fisica;
	}

	/**
	 * @return the nome_pessoa
	 */
	public String getNome_pessoa() {
		return nome_pessoa;
	}

	/**
	 * @param nome_pessoa the nome_pessoa to set
	 */
	public void setNome_pessoa(String nome_pessoa) {
		this.nome_pessoa = nome_pessoa;
	}

	/**
	 * @return the cpf_pessoa
	 */
	public String getCpf_pessoa() {
		return cpf_pessoa;
	}

	/**
	 * @param cpf_pessoa the cpf_pessoa to set
	 */
	public void setCpf_pessoa(String cpf_pessoa) {
		this.cpf_pessoa = cpf_pessoa;
	}

	/**
	 * @return the reg_pessoa
	 */
	public String getReg_pessoa() {
		return reg_pessoa;
	}

	/**
	 * @param reg_pessoa the reg_pessoa to set
	 */
	public void setReg_pessoa(String reg_pessoa) {
		this.reg_pessoa = reg_pessoa;
	}

	/**
	 * @return the nat_pessoa
	 */
	public String getNat_pessoa() {
		return nat_pessoa;
	}

	/**
	 * @param nat_pessoa the nat_pessoa to set
	 */
	public void setNat_pessoa(String nat_pessoa) {
		this.nat_pessoa = nat_pessoa;
	}

	/**
	 * @return the std_civil_pessoa
	 */
	public String getStd_civil_pessoa() {
		return std_civil_pessoa;
	}

	/**
	 * @param std_civil_pessoa the std_civil_pessoa to set
	 */
	public void setStd_civil_pessoa(String std_civil_pessoa) {
		this.std_civil_pessoa = std_civil_pessoa;
	}

	/**
	 * @return the dta_nsc_pessoa
	 */
	public Calendar getDta_nsc_pessoa() {
		return dta_nsc_pessoa;
	}

	/**
	 * @param dta_nsc_pessoa the dta_nsc_pessoa to set
	 */
	public void setDta_nsc_pessoa(Calendar dta_nsc_pessoa) {
		this.dta_nsc_pessoa = dta_nsc_pessoa;
	}

	/**
	 * @param enderecos the enderecos to set
	 */
	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}

}